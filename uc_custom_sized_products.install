<?php

/**
 * @file
 * Ubercart custom sized products install file.
 */

/**
 * Implements hook_install().
 */
function uc_custom_sized_products_install() {


  // Insert length, width and height variables into the uc_attributes table.
  // Add our length attribute.
  $length_aid = db_insert('uc_attributes')
    ->fields(array(
      'name' => 'uc_custom_sized_products_length',
      'label' => 'Length',
      'ordering' => -25,
      'required' => 1,
      'display' => 0,
      'description' => '',
      ))
    ->execute();

  // Add our width attribute.
  $width_aid = db_insert('uc_attributes')
    ->fields(array(
      'name' => 'uc_custom_sized_products_width',
      'label' => 'Width',
      'ordering' => -24,
      'required' => 1,
      'display' => 0,
      'description' => '',
      ))
    ->execute();

  // Add our height attribute.
  $height_aid = db_insert('uc_attributes')
    ->fields(array(
      'name' => 'uc_custom_sized_products_height',
      'label' => 'Height',
      'ordering' => -23,
      'required' => 1,
      'display' => 0,
      'description' => '',
      ))
    ->execute();

  variable_set('uc_custom_sized_products_length_aid', $length_aid);
  variable_set('uc_custom_sized_products_width_aid', $width_aid);
  variable_set('uc_custom_sized_products_height_aid', $height_aid);

  // Changing the weight of the module to be lower than uc_attribute.
  $weight = db_query("SELECT weight FROM {system} WHERE type = 'module' AND name = 'uc_attribute'")->fetchCol();
  $new_weight = $weight[0] - 1;
  
  $num_updated = db_update('system')
    ->fields(array(
      'weight' => $new_weight,
    ))
    ->condition('type', 'module')
    ->condition('name', 'uc_custom_sized_products')
    ->execute();
}

/**
 * Implements hook_uninstall().
 */
function uc_custom_sized_products_uninstall() {

  // Load the products that currently use this feature.
  $features = db_query("SELECT nid, pfid FROM {uc_custom_sized_products}");

  $nids = array();
  $pfids = array();

  foreach ($features as $feature) {
    $nids[] = $feature->nid;
    $pfids[] = $feature->pfid;
  }

  // Setup our attributes array.
  $aids[] = variable_get('uc_custom_sized_products_length_aid', '');
  $aids[] = variable_get('uc_custom_sized_products_width_aid', '');
  $aids[] = variable_get('uc_custom_sized_products_height_aid', '');


  // Remove all attributes from the uc_product_attributes table.
  if (sizeof($nids) && sizeof($aids)) {
    db_delete('uc_product_attributes')
    ->condition('nid', $nids, 'IN')
    ->condition('aid', $aids, 'IN')
    ->execute();
  }

  // Remove all features from the uc_product_features table.
  if (sizeof($nids) && sizeof($pfids)) {
    db_delete('uc_product_features')
    ->condition('nid', $nids, 'IN')
    ->condition('pfid', $pfids, 'IN')
    ->execute();
  }

  // Remove our attributes.
  if (sizeof($aids)) {
    db_delete('uc_attributes')
    ->condition('aid', $aids, 'IN')
    ->execute();
  }

  variable_del('uc_custom_sized_products_length_aid');
  variable_del('uc_custom_sized_products_width_aid');
  variable_del('uc_custom_sized_products_height_aid');

}

/**
 * Implements hook_schema().
 */
function uc_custom_sized_products_schema() {
  $schema['uc_custom_sized_products'] = array(
    'fields' => array(
      'vid'      => array('type' => 'int', 'unsigned' => TRUE, 'not null' => TRUE, 'default' => 0),
      'nid'      => array('type' => 'int', 'unsigned' => TRUE, 'not null' => TRUE, 'default' => 0),
      'pfid'      => array('type' => 'int', 'unsigned' => TRUE, 'not null' => TRUE, 'default' => 0),
      'measurement'    => array('type' => 'varchar', 'length' => 255, 'not null' => TRUE, 'default' => ''),
      'units'    => array('type' => 'varchar', 'length' => 255, 'not null' => TRUE, 'default' => ''),
      'length_min' => array('type' => 'numeric', 'size' => 'normal', 'not null' => TRUE, 'default' => 0, 'precision' => 7, 'scale' => 5),
      'width_min' => array('type' => 'numeric', 'size' => 'normal', 'not null' => TRUE, 'default' => 0, 'precision' => 7, 'scale' => 5),
      'height_min' => array('type' => 'numeric', 'size' => 'normal', 'not null' => TRUE, 'default' => 0, 'precision' => 7, 'scale' => 5),
      'allow_float' => array('type' => 'int', 'unsigned' => TRUE, 'not null' => TRUE, 'default' => 0, 'size' => 'tiny'),
    ),
    'primary key' => array('pfid', 'vid', 'nid'),
  );

  return $schema;
}

