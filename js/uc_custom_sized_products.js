(function ($) {
  
  Drupal.behaviors.uc_custom_sized_products = {
    attach: function (context, settings) {
      
      // Hold our attribute variables for improved javascript performance.
      var ucsp_length = $('#edit-attributes-' + Drupal.settings.ucsp.ucsp_length_aid);
      var ucsp_width = $('#edit-attributes-' + Drupal.settings.ucsp.ucsp_width_aid);
      var ucsp_height = $('#edit-attributes-' + Drupal.settings.ucsp.ucsp_height_aid);
      var ucsp_measurement = Drupal.settings.ucsp.ucsp_measurement;
      var $estimate_area = $('#ubercart-custom-sized-product-preview');
    
      // Common function to display prices to the user.
      var ucsp_update_price = function(data) {
        $('#ubercart-custom-sized-product-preview').html(data.body);
        $('#ubercart-custom-sized-product-preview').slideDown();
      }
    
      // Common function to run the ajax estimate.
      function ucsp_get_estimate(data){
        switch(ucsp_measurement){
        case 'linear':
          // If the length value is empty, don't get the estimated.
          if(ucsp_length.val() == '') {
            return false;
          }
          break;
        case 'area':
          // If either of the length and width values are empty,
          // don't get the estimate.
          if(ucsp_length.val() == '' || ucsp_width.val() == '') {
            return false;
          }
          break;
        case 'volume':
          // If any of the length, width and height values are empty,
          // don't get the estimate.
          if(ucsp_length.val() == '' || ucsp_width.val() == '' || ucsp_height.val() == '') {
            return false;
          }
          break;
        default:
          return false;
          break;
        }
        
        new Drupal.ajax('#ubercart-custom-sized-product-preview', $estimate_area, {
          url: Drupal.settings.basePath + '?q=uc_custom_sized_products/calculate-price/' + Drupal.settings.ucsp.ucsp_nid + '/' + ucsp_length.val() + '/' + ucsp_width.val() + '/' + ucsp_height.val(),
          settings: {},
          event: 'click tap'
        });
        
      }
    
      $('#edit-attributes-' + Drupal.settings.ucsp.ucsp_length_aid,context).change(function(){
        // Clear the preview pane
        $('#ubercart-custom-sized-product-preview').empty().slideUp();
          ucsp_get_estimate();
          // Return false so the navigation stops here and does not
          // continue to the page in the link.
          return false;
        });
    
        $('#edit-attributes-' + Drupal.settings.ucsp.ucsp_width_aid,context).change(function(){
          $('#ubercart-custom-sized-product-preview').empty().slideUp()
            ucsp_get_estimate();
            // Return false so the navigation stops here and does not
            // continue to the page in the link.
            return false;
        });
    
        $('#edit-attributes-' + Drupal.settings.ucsp.ucsp_height_aid,context).change(function(){
          $('#ubercart-custom-sized-product-preview').empty().slideUp()
            ucsp_get_estimate();
            // Return false so the navigation stops here and does not
            // continue to the page in the link.
            return false;
        });
      
    }
  };
  
})(jQuery);

