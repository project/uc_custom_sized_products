Ubercart Custom Sized Products

The Ubercart Custom Sized Products module will allow store owners to sell
products with custom dimensions. For example, if the store wanted to sell carpet
by the square foot or to sell rope by the foot. The module will calculate the
length, area, or volume and will multiply that number by the price set for the
product.

The module utilizes Ubercart's Features. 

--

Installation Instructions:

1. Download the module and place it here: /sites/all/modules
2. Enable the module here: admin/build/modules
3. Create a new product node or edit an existing product node and click on the
   features link (node/<NID>/edit/features)
4. Select 'Custom Sized Product' from the 'Add new feature' select menu and
   click 'Add'
  4a. Choose your measurement (linear, area, or volume).
  4b. Select your unit of measurement (feet, inches, yards, centimeters,
      millimeters, meters).
  4c. Enter any minimums for length, width or height.
  4d. Set the price you want to sell units for (calculations are):
      - Linear: length * sell_price
      - Area: (length * width) * sell_price
      - Volume: (length * width * height) * sell_price
5. Save the feature..
